require 'drb/drb'
require './core/storage'

base_file_path = 'files/base-dictionary.txt'
storage = Storage.new.load_from_file(base_file_path)

DRb.start_service('druby://localhost:9000', storage)
DRb.thread.join