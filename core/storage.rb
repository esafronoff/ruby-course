﻿# coding: UTF-8

require 'rubygems'
require 'zip/zipfilesystem'

class Storage
    def initialize
        @root_node = assign_node('', [], false)
    end

    def add(string)
        words = string.delete("' '||'\n'").split(',')
        words.each do |word|
            add_word(word)
        end
        self
    end

    def contains?(key)
        current_node = @root_node
        key.each_char.with_index do |c, i|
            new_node = find_node_in_array(current_node[:childList], c)
            if new_node
                current_node = new_node
                return true if current_node[:isEnd] && i == key.length-1
            else
                return false
            end
        end
    end

    def find(prefix)
       raise ArgumentError, 'Length of prefix should be 3 or great' if prefix.length < 3 
       current_node = @root_node
       prefix.each_char.with_index do |c, i| 
           new_node = find_node_in_array(current_node[:childList], c)
           if new_node
               current_node = new_node
               return get_all_tail_by_node('', current_node, []).map{|tail| prefix.chop + tail} if i == prefix.length-1
           else
               return []
           end
       end
    end

    private def get_all_tail_by_node(tail, node, acc)
        if node[:isEnd]
            acc << tail + node[:label]
            if node[:childList].any?
              tail += node[:label]
              node[:childList].each do |n|
                get_all_tail_by_node(tail, n, acc)
              end
            else
                return acc
            end
        else
            tail += node[:label]
            node[:childList].each do |n|
              get_all_tail_by_node(tail, n, acc)
            end
        end
        acc
    end

    def add_word(word)
        current_node = @root_node
        word.each_char.with_index do |c, i|
            new_node = find_node_in_array(current_node[:childList], c)
            if new_node
                current_node = new_node
                current_node[:isEnd] = true if i == word.length-1
            else
                new_node = assign_node(c, [], i == word.length-1)
                current_node[:childList] << new_node
                current_node = new_node
            end
        end
    end

    private def find_node_in_array(list, label)
        list.each do |n|
            return n if (n[:label] == label)
        end
        nil
    end

    private def assign_node(label, child_list, is_end)
        node = Hash.new
        node[:label] = label
        node[:childList] = child_list
        node[:isEnd] = is_end
        node
    end

    def to_s
        @root_node
    end

    def load_from_file(filename)
        f = File.open(filename, 'r')
        f.each_line do |line|
            add(line)
        end
        f.close
        self
    end

    def load_from_zip(zipfile)
        Zip::ZipFile.open(zipfile) do |file|
            file.each do |content|
                text = file.read(content)
                text.each_line do |line|
                    add(line)
                end
            end
        end
    end

    def save_to_file(filename)
       all_keys = get_all_tail_by_node('', @root_node, []).join(',')
       file = File.open(filename, 'w')
       file.write(all_keys) 
       file.close
    end

    def save_to_zip(zipfile_name)
        filename = 'core/files/tmp/output.txt'
        save_to_file(filename)
        Zip::ZipFile.open(zipfile_name, Zip::ZipFile::CREATE) do |zipfile|
                zipfile.add(filename,filename)
        end
    end
end