# DataArt Ruby Course

This study project for DataArt Ruby Course

@author: Evgene Safronov
   
## Folder structure 

    /core  - base directory with Storage class and tests
        /test - tests for class Storage
        /coverage - tests report
        /files - directory for working with files
    /files - example files with load data
    /storage-da-es - folder with gem
    server.rb - sinatra application
    drb-server.rb - drb service for Storage class
    srb-client.rb - client for testing drb service

## Rake task
Run tests for class that in path: core/test/storage_test.rb  
  
    $rake test

Run sinatra application server on port 4567

    $rake server

Run DRB service on port 9000

    $rake drbserver
    
Run client for listen DRB service on port 9000
     
    $rake drbclient


## Ruby gem for Storage

I create ruby gem with name storage-da-es: https://rubygems.org/gems/storage-da-es

So you can install this one

    $gem install storage-da-es

This gem provide class Storage and base in folder storage-da-es
You can use this in code:
    
    require 'storage/da/es'
    @storage = Storage::Da::Es::Storage.new 


