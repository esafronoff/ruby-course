require 'rubygems'
require 'test_helper'

class Storage::Da::EsTest < Minitest::Test
  def setup
    @storage = Storage::Da::Es::Storage.new
  end

  def test_empty_value
    empty_value = {:label=>'', :childList=>[], :isEnd=>false}
    assert_equal empty_value, @storage.to_s
  end

  def test_add_new_value
    @storage.add('abc')
    expected_value = {:label=>'', :childList=>[{:label=>'a', :childList=>[{:label=>'b', :childList=>[{:label=>'c', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}], :isEnd=>false}
    assert_equal expected_value, @storage.to_s
  end

  def test_contains_key
    assert_equal false, @storage.contains?(Time.now.to_f.to_s)
    @storage.add('testkey')
    assert_equal true, @storage.contains?('testkey')
  end

  def test_find_key_by_prefix
    assert_empty @storage.find('abcdefg')

    @storage.add('abcd,abcfop')
    assert_equal ['abcd', 'abcfop'], @storage.find('abc')

    @storage.add('zxc')
    assert_equal ['zxc'], @storage.find('zxc')

    assert_raises ArgumentError do
      @storage.find('12')
    end
  end

  def test_load_data_from_file
    @storage.load_from_file('test/mocks/input.mock.txt')
    expected_value = {:label=>'', :childList=>[{:label=>'a', :childList=>[{:label=>'b', :childList=>[{:label=>'c', :childList=>[], :isEnd=>true}, {:label=>'n', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}, {:label=>'s', :childList=>[{:label=>'d', :childList=>[{:label=>'f', :childList=>[], :isEnd=>true}], :isEnd=>true}], :isEnd=>false}, {:label=>'f', :childList=>[{:label=>'g', :childList=>[{:label=>'h', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}], :isEnd=>false}
    assert_equal expected_value, @storage.to_s
  end

  def test_save_to_file
    @storage.add('abc,ab,dfg,how,do')
    @storage.save_to_file('test/mocks/output.mock.txt')
    @storage.load_from_file('test/mocks/output.mock.txt')
    expected_value = {:label=>'', :childList=>[{:label=>'a', :childList=>[{:label=>'b', :childList=>[{:label=>'c', :childList=>[], :isEnd=>true}], :isEnd=>true}], :isEnd=>false}, {:label=>'d', :childList=>[{:label=>'f', :childList=>[{:label=>'g', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'o', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'h', :childList=>[{:label=>'o', :childList=>[{:label=>'w', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}], :isEnd=>false}
    assert_equal expected_value, @storage.to_s
  end

  def test_load_data_from_zip_file
    @storage.load_from_zip('test/mocks/in.zip')
    expected_value = {:label=>'', :childList=>[{:label=>'a', :childList=>[{:label=>'x', :childList=>[{:label=>'v', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'b', :childList=>[{:label=>'c', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'d', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}
    assert_equal expected_value, @storage.to_s
  end

  def test_save_data_to_zip_file
    @storage.add('abc,ab,dfg,how,do')
    File.delete('test/mocks/out.zip') if File.exist?('test/mocks/out.zip')
    @storage.save_to_zip('test/mocks/out.zip')
    @storage.load_from_zip('test/mocks/out.zip')
    expected_value = {:label=>'', :childList=>[{:label=>'a', :childList=>[{:label=>'b', :childList=>[{:label=>'c', :childList=>[], :isEnd=>true}], :isEnd=>true}], :isEnd=>false}, {:label=>'d', :childList=>[{:label=>'f', :childList=>[{:label=>'g', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'o', :childList=>[], :isEnd=>true}], :isEnd=>false}, {:label=>'h', :childList=>[{:label=>'o', :childList=>[{:label=>'w', :childList=>[], :isEnd=>true}], :isEnd=>false}], :isEnd=>false}], :isEnd=>false}
    assert_equal expected_value, @storage.to_s
  end
end