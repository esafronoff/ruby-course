$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'storage/da/es'

require 'minitest/autorun'

require 'simplecov'
SimpleCov.start do
  coverage_dir 'test/coverage'
end
SimpleCov.minimum_coverage_by_file 80

require 'zip/zipfilesystem'
