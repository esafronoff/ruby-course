require 'rubygems'
require 'sinatra'
require './core/storage'
require 'json'

base_file_path = 'files/base-dictionary.txt';
storage = Storage.new.load_from_file(base_file_path)

post '/add' do
  content_type  :json
  storage.add(params[:string] || '')
end

get '/contains' do
  content_type  :json
  { :result => storage.contains?(params[:string]) }.to_json
end

get '/find' do
  content_type :json
  {words: storage.find(params[:string])}.to_json
end