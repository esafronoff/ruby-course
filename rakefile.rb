task :default => [:test]

task :test do
  ruby 'core/test/storage_test.rb'
end

task :server do
  ruby 'server.rb'
end

task :drbserver do
  ruby 'drb-server.rb'
end

task :drbclient do
  ruby 'drb-client.rb'
end
